import {sleep, check} from "k6";
import { Counter } from "k6/metrics";
import http from "k6/http";

let errorCounter = new Counter('errror_postOrder');

var dataTest = require('../../data/order/dataOrder.js');
var dataAuth = require('../../data/dataMaster.js');

export const options = {
    scenarios: {
        constant_request_rate: {
            executor: 'constant-arrival-rate',
            rate: 10,
            timeUnit: '1s',
            duration: '10s',
            preAllocatedVUs: 5,
            maxVUs: 10
        },
    }
}

const request = () => {
    var params = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${dataAuth.tokenUAT}`
        }
    };

    var res = http.post(dataAuth.baseUrl+"/v2/orders/batch", dataTest.payloadOrder, params);

    if (res.status !== 201) {
        errorCounter.add(1)
    }

    return res;
}

export default function() {
    check(request(), {
        "is status 201": r => r.status === 201
    });

    sleep(1);
}