import http from 'k6/http';
import { check } from 'k6';

var dataAuth = require('../../data/dataMaster.js');
export const options = {
    scenarios: {
        constant_request_rate: {
            executor: 'constant-arrival-rate',
            rate: 10,
            timeUnit: '1s',
            duration: '10s',
            preAllocatedVUs: 5,
            maxVUs: 10
        },
    }
}

var dataAuth = require('../../data/dataMaster.js');

// export const options = {
//     stages: [
//         { target: 5, duration: '1m' },
//         { target: 5, duration: '2m' },
//         { target: 0, duration: '1m' },
//     ],
//     thresholds: {
//         "http_req_duration": ["p(95)<1000"]
//     },
// };

export default function () {

    let requests = {
        'v1/department': {
            method: 'GET',
            url: dataAuth.baseUrl+'/v1/department',
            params: {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${dataAuth.tokenUAT}`
                }
            },
        },
        'user/related-data': {
            method: 'GET',
            url: dataAuth.baseUrl+'/user/related-data',
            params: {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${dataAuth.tokenUAT}`
                }
            },
        },
        'v1/finance/balance/tenant-balance': {
            method: 'GET',
            url: dataAuth.baseUrl+'/v1/finance/balance/tenant-balance',
            params: {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${dataAuth.tokenUAT}`
                }
            },
        },
        'v2/orders/payment-method': {
            method: 'GET',
            url: dataAuth.baseUrl+'/v2/orders/payment-method',
            params: {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${dataAuth.tokenUAT}`
                }
            },
        },
        'v1/notification': {
            method: 'GET',
            url: dataAuth.baseUrl+'/v1/notification',
            params: {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${dataAuth.tokenUAT}`
                }
            },
        },
        'v1/orders': {
            method: 'POST',
            url: dataAuth.baseUrl+'/v1/orders?take=30&skip=0&includeTotalCount=true&startDate=2022-04-11&endDate=2022-05-11',
            params: {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${dataAuth.tokenUAT}`
                }
            },
        },
        'v1/apps': {
            method: 'GET',
            url: dataAuth.baseUrl+'/v1/apps',
            params: {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${dataAuth.tokenUAT}`
                }
            },
        },
        'v1/branch': {
            method: 'GET',
            url: dataAuth.baseUrl+'/v1/branch',
            params: {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${dataAuth.tokenUAT}`
                }
            },
        }
    };
    let responses = http.batch(requests);

    check(responses['v1/department'], {
        'v1/department status was 200': (res) => res.status === 200,
    });

    check(responses['user/related-data'], {
        'user/related-data status was 201': (res) => res.status === 201,
    });

    check(responses['v1/finance/balance/tenant-balance'], {
        'v1/finance/balance/tenant-balance status was 200': (res) => res.status === 200,
    });

    check(responses['v2/orders/payment-method'], {
        'v2/orders/payment-method status was 200': (res) => res.status === 200,
    });

    check(responses['v1/notification'], {
        'v1/notification status was 200': (res) => res.status === 200,
    });

    check(responses['v1/orders'], {
        'v1/orders': (res) => res.status === 200,
    });


    check(responses['v1/apps'], {
        'v1/apps': (res) => res.status === 200,
    });
  
    check(responses['v1/branch'], {
        'v1/branch': (res) => res.status === 200,
    });
}