import {sleep, check} from "k6";
import { Counter } from "k6/metrics";
import http from "k6/http";

let errorCounter = new Counter('errror_getDepartment');
var dataAuth = require('../../data/dataMaster.js');

export const options = {
    scenarios: {
        constant_request_rate: {
            executor: 'constant-arrival-rate',
            rate: 10,
            timeUnit: '1s',
            duration: '10s',
            preAllocatedVUs: 5,
            maxVUs: 10
        },
    }
}

const request = () => {
    var params = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${dataAuth.tokenUAT}`
        }
    };

    var res = http.get(dataAuth.baseUrl+"/v1/department", params);

    if (res.status !== 200) {
        errorCounter.add(1)
    }

    return res;
}

export default function() {
    check(request(), {
        "is status 200": r => r.status === 200
    });

    sleep(1);
}