import {sleep, check} from "k6";
import { Counter } from "k6/metrics";
import http from "k6/http";

let errorCounter = new Counter('errror_putPayment');

var dataTest = require('../../data/payment/dataPayment.js');
var dataAuth = require('../../data/dataMaster.js');

// export const options = {
//     scenarios: {
//         constant_request_rate: {
//             executor: 'constant-arrival-rate',
//             rate: 20,
//             timeUnit: '1s',
//             duration: '10s',
//             preAllocatedVUs: 1,
//             maxVUs: 5
//         },
//     }
// }

export const options = {
    stages: [
      { duration: '5s', target: 5 }, // simulate ramp-up of traffic from 1 to 60 users over 5 minutes.
      { duration: '10s', target: 5 }, // stay at 60 users for 10 minutes
      { duration: '3s', target: 10 }, // ramp-up to 100 users over 3 minutes (peak hour starts)
      { duration: '2s', target: 10 }, // stay at 100 users for short amount of time (peak hour)
      { duration: '3s', target: 5 }, // ramp-down to 60 users over 3 minutes (peak hour ends)
      { duration: '10s', target: 5 }, // continue at 60 for additional 10 minutes
      { duration: '5s', target: 0 }, // ramp-down to 0 users
    ]
}

const request = () => {
    var params = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${dataAuth.tokenUAT}`
        }
    };

    var res = http.put(dataAuth.baseUrl+"/v1/payment/4866", dataTest.payloadPutPayment, params);

    if (res.status !== 200) {
        errorCounter.add(1)
    }

    return res;
}

export default function() {
    check(request(), {
        "is status 200": r => r.status === 200
    });

    sleep(1);
}