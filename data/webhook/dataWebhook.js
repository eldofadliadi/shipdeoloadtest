exports.payloadWebhook = JSON.stringify({
    "airwayBillNumber": "002255009501",
    "courierChannelId": "sicepat",
    "lastStatus": "delivered",
    "lastStatusDateTime": null,
    "note": "Paket telah di pick up oleh [SIGESIT - Adis Susanto]",
    "receiverName": null,
    "receiverRelation": null,
    "status": "delivering",
    "statusDateTime": "2021-09-21T14:42:00+07:00",
    "tenantId": "37645"
});